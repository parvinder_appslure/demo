import React, {useRef} from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Splash from './splash';
import Home from './home';
import {Image, TouchableOpacity, View} from 'react-native';
import {imageStyle, viewStyle} from './style';
const iconPath = {
  h: require('./src/assets/Icons/home.png'),
  ha: require('./src/assets/Icons/home2.png'),
  f: require('./src/assets/Icons/following.png'),
  fa: require('./src/assets/Icons/following2.png'),
  m: require('./src/assets/Icons/message.png'),
  ma: require('./src/assets/Icons/message2.png'),
  p: require('./src/assets/Icons/profile.png'),
  pa: require('./src/assets/Icons/profile2.png'),
};
const TabIcon = (source) => (
  <Image source={source} style={imageStyle.tabIcon} />
);
const TabBarButton = (onPress, isFocused) => {
  if (isFocused) {
    return <></>;
  } else {
    return (
      <TouchableOpacity style={viewStyle.tabButton} onPress={onPress}>
        <Image
          source={require('./src/assets/Icons/video.png')}
          style={imageStyle.tabButton}
        />
      </TouchableOpacity>
    );
  }
};
const Tab = createBottomTabNavigator();
const TabNavigator = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        showLabel: false,
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.ha : iconPath.h),
        }}
      />
      <Tab.Screen
        name="Following"
        component={Home}
        options={{
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.fa : iconPath.f),
        }}
      />
      <Tab.Screen
        name="Video"
        component={Home}
        options={({navigation}) => ({
          tabBarButton: () =>
            TabBarButton(
              () => navigation.navigate('Video'),
              navigation.isFocused(),
            ),
          tabBarVisible: false,
        })}
      />
      <Tab.Screen
        name="Message"
        component={Home}
        options={{
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.ma : iconPath.m),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Home}
        options={{
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.pa : iconPath.p),
        }}
      />
    </Tab.Navigator>
  );
};
const Stack = createStackNavigator();
const Navigator = () => {
  const navigationRef = useRef();
  // useReduxDevToolsExtension(navigationRef);
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator
        // initialRouteName={'Store'}
        screenOptions={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}>
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="TabNavigator"
          component={TabNavigator}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigator;
