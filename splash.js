import React, {useEffect} from 'react';
import {Text, View} from 'react-native';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('TabNavigator');
    }, 2000);
  }, []);
  return (
    <View>
      <Text>splash</Text>
    </View>
  );
};
export default Splash;
